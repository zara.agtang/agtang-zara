import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css'

const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);





//SAMPLE CODE ONLY
/*const name = 'Ada Lovelace';
const user = {
  firstName: "Jane",
  lastName: "Smith"
}

function formatName(user){
  return user.firstName + " " + user.lastName
}

const element = <h1>Hello, {formatName(user)}</h1>; //JSX - JavaScript XML

const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(element);*/