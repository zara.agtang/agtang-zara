import { Card, Button } from 'react-bootstrap'

export default function CourseCard() {

	return (

		<Card id="courseComponent1">
			<Card.Body className="mt-3 mb-3">
				<Card.Title>Sample Course</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>This is a sample course offering.</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>PhP 40,000</Card.Text>
				
				<Button variant="primary">Enroll</Button>
			</Card.Body>
		</Card>

	)
}