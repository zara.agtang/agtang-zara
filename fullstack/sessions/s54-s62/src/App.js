import './App.css';
import AppNavbar from './components/AppNavbar'
import Container from 'react-bootstrap/Container'

import Home from './pages/Home'

function App() {
  return (
    //React Fragments <> </>
    <>
      <AppNavbar />
      <Container>
          <Home />
      </Container>
    </>
  );
}

export default App;
