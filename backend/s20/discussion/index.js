console.log("Hello World")

//Arithmetic Operators
//+,-,*,/,% (modulo operator)

	let x = 1397;
	let y = 7831;

	let sum = x + y;
	console.log("Result of addition operator: " + sum);

	//difference
	let difference = x - y;
	console.log("Result of subtraction operator: " + difference);

	//product
	let product = x * y;
	console.log("Result of multiplication operator: " + product);

	//quotient
	let quotient = x / y;
	console.log("Result of division operator: " + quotient);

	//remainder
	let remainder = x % y;
	console.log("Result of modulo operator: " + remainder);

	let a = 10;
	let b = 5;

	let remainderA = a % b;
	console.log(remainderA);//0

//Assignment Operator
//Basic Assignment operator (=)

let assignmentNumber = 8;

//Addition Assignment Operator (+=)

assignmentNumber = assignmentNumber + 2;//10

//10+2
assignmentNumber += 2;//12
console.log(assignmentNumber);

//Subtraction/Multiplication/Division Assignment operator

assignmentNumber -= 2;//10
console.log("Result of -= : " + assignmentNumber);
assignmentNumber *= 2;//20
console.log("Result of *= : " + assignmentNumber);
assignmentNumber /= 2;//10
console.log("Result of /= : " + assignmentNumber);

//Multiple Operators and Parentheses

/*
	1. 3 * 4 = 12;
	2. 12 / 5 = 2.4
	3. 1 + 2 = 3
	4. 3 - 2.4 = 0.6
*/

let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas);

/*
	1. 4/5 = 0.8
	2. 2-3 = -1
	3. -1 * 0.8 = -0.8
	4. 1 + -0.8 = 0.2
*/


let pemdas = 1 + (2-3) * (4/5);
console.log(pemdas);

//Increment and Decrement
//Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to

let z = 1;

	let increment = ++z;

	//the value of "z" is added by a value of one before returning the value and storing it in the variable "increment"

	//increase then re-assign

	console.log("Result of pre-increment: " + increment);
	console.log("Result of pre-increment: " + z);

	//the value of "z" is returned and storred in the variable "increment" then the value of "z" is increased by one

	//re-assign then increase

	increment = z++;//1 = 1++
	console.log("Result of post-increment: " + increment);
	console.log("Result of post-increment: " + z);


//pre-decrement
	//decrease then re-assign
	let decrement = --z;
	console.log("Result of pre-decrement: " + decrement);
	console.log("Result of pre-decrement: " + z);

//post-decrement
	//re-assign then decrease
	decrement = z--;
	console.log("Result of post-decrement: " + decrement);
	console.log("Result of post-decrement: " + z);

//Type Coercion
/*
	Type Coercion is the automatic or implicit conversion of values from one data type to one another
*/

let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion);//"1012"
console.log(typeof coercion);

let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

//the boolean "true" is associated with the value of 1

let numE = true + 1;
console.log(numE);

//the boolean "false" is associated with the value of 0

let numF = false + 1;
console.log(numF);

//Comparison Operators

let juan = 'juan';

//Equality Operator (==)

console.log(1 == 1);//true
console.log(1 == 2);//false
console.log(1 == '1');//true
console.log(0 == false);//true
console.log('juan' == 'juan');//true
console.log('juan' == juan);//true

//Inequality Operator (!=)

console.log(1 != 1);//false
console.log(1 != 2);//true
console.log(1 != '1');//false
console.log(0 != false);//false
console.log('juan' != 'juan');//false
console.log('juan' != juan);//false



//Strict Equality Operator (===)
console.log("===");
console.log(1 === 1);//true
console.log(1 === 2);//false
console.log(1 === '1');//false
console.log(0 === false);//false
console.log('juan' === 'juan');//true
console.log('juan' === juan);//true

//Strict Inequality Operator (!==)
console.log("!==");
console.log(1 !== 1);//false
console.log(1 !== 2);//true
console.log(1 !== '1');//true
console.log(0 !== false);//true
console.log('juan' !== 'juan');//false
console.log('juan' !== juan);//false



//Relational Operators

let abc = 50;
let def = 65;

let isGreaterThan = abc > def;//false
let isLessThan = abc < def;//true
let isGTOrEqual = abc >= def;//false
let isLTOrEqual = abc <= def;//true

console.log(isGreaterThan);
console.log(isLessThan);
console.log(isGTOrEqual);
console.log(isLTOrEqual);

let numStr = "30";
console.log(abc > numStr);//true; forced coercion to change the string to a number
console.log(def <= numStr);//false

let str = "twenty";
console.log( def >= str);


//Logical Operators

let isLegalAge = true;
let isRegistered = false;

//&& (AND), || (OR),  ! (NOT)

//&&
//Return true if all operands are true

let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of AND operator: " + allRequirementsMet);//false

//||
//Returns true if ONE of the operands are true

let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of OR operator: " + someRequirementsMet);//true

//!
//Returns the opposite value

let someRequirementsNotMet = !isRegistered;
console.log("Result of NOT operator: " + someRequirementsNotMet);//true