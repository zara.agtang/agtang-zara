//CRUD Operations
/*
	- CRUD operations are the heart of any backend application
	- Mastering the CRUD operations is essential for any developer
	-Mastering the CRUD operations of any language makes us a valuable developer and makes the work easier for us to deal with huge amounts of information
*/

//to switch dbs, we use - use <dbName>

//[Create] Insert documents
	//Insert one document
	
		/*
			- Since mongoDB deals with objects as it's structure for documents, we can easily create them by providing objects into our methods.
			- The mongo shell also uses JavaScript for it's syntax which makes it convenient for us to understand it's code
			- Creating MongoDB syntax in a text editor makes it easy for us to modify and create our code as opposed to typing it directly in the terminal where the whole code is only visible in one line.
			- By using a text editor it allows us to type the syntax using multiple lines and simply copying and pasting the code in terminal will make it work.

		*/

		//Syntax:
			//db.collectionName.insertOne({object});
	
	db.users.insertOne({
		"firstName":"John",
		"lastName":"Smith"
	})

	db.users.insertOne({
		firstName: "Jane",
		lastName: "Doe",
		age: 21,
		contact:{
			phone: "87000",
			email:"janedoe@gmail.com"
		},
		courses:["CSS","JavaScript","Python"],
		department: "none"
	})

	//Insert Many
	/*
		Syntax
			db.collectionName.insertMany([{objectA},{objectB}])
	*/

	db.users.insertMany([
			{
				firstName: "Stephen",
				lastName: "Hawking",
				age: 76,
				contact:{
					phone: "87001",
					email:"stephenhawking@gmail.com"
				},
				courses:["Python","React","PHP"],
				department: "none"			
			},
			{
				firstName:"Neil",
				lastName:"Armstrong",
				age: 82,
				contact: {
					phone: "87002",
					email:"neilarmstrong@gmail.com"
				},
				courses:["React","Laravel","Sass"],
				department: "none"
			}
		])


//[Read] Finding Documents

//Find

	/*
		- If multiple documents match the criteria for finding a document only the FIRST document that matches the search term will be returned
		- This is based from the order that documents are stored in a collection
		- If a document is not found, the terminal will respond with a blank line

	*/
	/*
		Syntax:
		db.collectionName.find()
		db.collectionName.find({field:value})
	*/

//Finding a single document
	//leaving the search criteria empty will retrieve ALL the documents
	db.users.find()

	db.users.find({firstName:"Stephen"})

//Finding document with multiple parameters
/*
	Syntax:
		db.collectionName.find({fieldA: valueA, fieldB:valueB})
*/

	db.users.find({lastName:"Armstrong",age:82})

//[Update] Updating a document


//Updating a single Document

	//let's add another document
	db.users.insertOne({
		firstName: "Test",
		lastName:"Test",
		age: 0,
		contact:{
			phone: "00000",
			email:"test@gmail.com"
		},
		courses: [],
		department: "none"
	})

	/*
		- Just like the "find" method, methods that only manipulate a single document will only update the FIRST document that matches the search criteria.
		Syntax:
			db.collectionName.updateOne({criteria},{$set:{field,value}});

	*/

	//$set operator replaces the value of a field with the specified value

	db.users.updateOne(
		{firstName: "Test"},
		{
			$set:{
				firstName: "Bill",
				lastName: "Gates",
				age: 65,
				contact:{
					phone: "12345678",
					email:"bill@gmail.com"
				},
				courses:["PHP","Laravel","HTML"],
				department:"Operations",
				status: "active"
			}
		}
	)
	
	db.users.find({firstName:"Bill"})
	//status is inserted automatically

//Updating multiple documents
/*
	Syntax:
		db.collectionName.updateMany({criteria},{$set:{field,value}})

		

*/

	db.users.updateMany(
		{department: "none"},

		{
			$set:{department:"HR"}
		}
	)



//[Delete] Deleting documents

//create a document to delete

	db.users.insert({
		firstName:"test"
	})

//Deleting a single document
	/*
		Syntax
		db.collectionName.deleteOne({criteria})
	*/

	db.users.deleteOne({
		firstName:"test"
	})

//Delete Many
	/*
		Be careful when using the deleteMany method. If we do not add a search criteria, it will DELETE ALL DOCUMENTS in a db
		DO NOT USE: databaseName.collectionName.deleteMany()
		Syntax:
		db.collectionName.deleteMany({criteria})

	*/

	db.users.deleteMany({
		firstName: "Bill"
	})


//[Take Home Review]
	//In your course booking db, add the following documents:

//[1# replaceOne]
	db.users.insertOne(

		{
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact:{
				phone: "12345678",
				email:"bill@gmail.com"
			},
			courses:["PHP","Laravel","HTML"],
			department:"Operations",
			status: "active"
		}

	)

// Replace One
/*
    - Can be used if replacing the whole document is necessary.
    - Syntax
        - db.collectionName.replaceOne( {criteria}, {$set: {field: value}});
*/
//[#2]
db.users.replaceOne(
    { firstName: "Bill" },
    {
        firstName: "Bill",
        lastName: "Gates",
        age: 65,
        contact: {
            phone: "12345678",
            email: "bill@gmail.com"
        },
        courses: ["PHP", "Laravel", "HTML"],
        department: "Operations"
    }
)


//[Advanced Queries]

/*
    - Retrieving data with complex data structures is also a good skill for any developer to have.
    - Real world examples of data can be as complex as having two or more layers of nested objects and arrays.
    - Learning to query these kinds of data is also essential to ensure that we are able to retrieve any information that we would need in our application
*/

// Query an embedded document
db.users.find({
    contact: {
        phone: "87654321",
        email: "stephenhawking@gmail.com"
    }
})

// Query on nested field
db.users.find(
    {"contact.email": "janedoe@gmail.com"}
)

// Querying an Array with Exact Elements
db.users.find( { courses: [ "CSS", "JavaScript", "Python" ] } )

// Querying an Array without regard to order
db.users.find( { courses: { $all: ["React", "Python"] } } )
//$all operator selects the documents where the value of a field is an array that contains all the specified elements. 


// Querying an Embedded Array
//#3
db.users.insert({
    namearr: [
        {
            namea: "juan"
        },
        {
            nameb: "tamad"
        }
    ]
})

db.users.find({
    namearr: 
        {
            namea: "juan"
        }
})