//JavaScript Synchronous vs Asynchronous
//JS by default is synchronous meaning that only one statement is executed at a time

/*console.log("Hey!");
cosnole.log("Hello from cosnole!");
console.log("I'll run if no error occurs!");*/

/*console.log("Hi before the loop")
for(let i=0; i<=5;i++){
	console.log(i);
}
console.log("Hi after the loop")*/

//Asynchronous means that we can proceed to execute other statements, while time consuming code is running in the background
//with the use of promise

//[Getting All Posts]
//The Fetch API allows us to asynchronously request for a resource(data)
//A "promise" is an OBJECT that represents the eventual completion (or failure) of an asynch function and its resulting value

//Promises is like a special container for a future value that may not be available immediately. The promise represents the outcome of an asynch operation that may take some time to complete, such as fetching data from a server, reading a file, getting data from the database and any other task that doesn't happen instantly.

//Syntax
	//fetch('URL')

console.log(fetch('https://jsonplaceholder.typicode.com/posts'))

//the fetch() function returns a Promise which can then be chained using the .then() function. The then() function waits for the promise to be resolved before executing the code

//Syntax
	//fetch('URL')
	//.then((response)=>{})

fetch('https://jsonplaceholder.typicode.com/posts')
.then(response =>console.log(response.status))

fetch('https://jsonplaceholder.typicode.com/posts')//100
//Use the 'json' method from the "Response" object to CONVERT the data retrieved into JSON format to be used in our application
.then((response)=>response.json())
//log the converted JSON value from the "fetch" request
//when we have multiple .then methods, it creates a promise chain
.then((posts)=>console.log(posts))

//you can't use async function without the "await" keyword
//One advantage of Async/await is it allows for writing asynchronous code in a more readable and synchronous-like manner, making it easier to understand and maintain compared to using .then for handling promises. Magiging apparent ito kung mayroon po tayong maraming fetch request sa isang function.

async function fetchData(){

	//waits for the "fetch" method to complete then stores the value in the "result" variable
	let result = await fetch('https://jsonplaceholder.typicode.com/posts')
	//result returned from fetch is a promise
	console.log(result);
	console.log(typeof result);
	console.log(result.body);

	//convert the data from the response object as JSON
	let json = await result.json()
	console.log(json);
}

fetchData()


//[Retrieves a specific post]
fetch('https://jsonplaceholder.typicode.com/posts/18')
.then((response)=>response.json())
.then((json)=>console.log(json))


//[Creating a post]
fetch('https://jsonplaceholder.typicode.com/posts/',{

	method: 'POST',
	headers: {
		'Content-type':'application/json'
	},
	body: JSON.stringify({
		title: 'New post',
		body: 'Hello world!',
		userId: 1
	})
})
.then((response)=>response.json())
.then((json)=>console.log(json));


//[Updating a post]
fetch('https://jsonplaceholder.typicode.com/posts/1',{

	method: 'PUT',
	headers: {
		'Content-type':'application/json'
	},
	body: JSON.stringify({
		title: 'Corrected post'
	})
})
.then((response)=>response.json())
.then((json)=>console.log(json));


//[Deleting a post]
fetch('https://jsonplaceholder.typicode.com/posts/1',{
	method: 'DELETE'
})


//[Filtering a post]
//Syntax
	//Individual parameters
		//'url?parameterName=value'
	//Multiple parameters
		//'url?paramA=valueA&paramB=valueB'

fetch('https://jsonplaceholder.typicode.com/posts?userId=1&id=6')
.then((response)=>response.json())
.then((json)=>console.log(json))


//[Retrieve nested/related comments to posts]
fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
.then((response)=>response.json())
.then((json)=>console.log(json))