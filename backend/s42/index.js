//Setup a basic Express JS Server

//Dependencies
const express = require("express");
const mongoose = require("mongoose");

const taskRoute = require("./routes/taskRoute");

//Server setup

const app = express();
const port = 4000;

//Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}))

//Database connection (Connect to MongoDB Atlas)
mongoose.connect("mongodb+srv://zaraagtang:admin123@batch-297.wldzis1.mongodb.net/taskDB?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;
db.on("error",console.error.bind(console, "connection error"))
db.once("open",()=>console.log("We're connected to the cloud database"))

app.use("/tasks", taskRoute);
//http:localhost4000/tasks


//Server listening
if(require.main === module){
	app.listen(port,()=>console.log(`Server running at port ${port}`));
}

module.exports = app;