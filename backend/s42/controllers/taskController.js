//Controllers contain the functions and business logic of our Express JS application

const Task = require("../models/task");

module.exports.getAllTasks = () =>{
	return Task.find({}).then(result=>{
		return result;
	})
}

module.exports.createTask = (requestBody) =>{

	let newTask = new Task({
		name: requestBody.name
	})

	return newTask.save().then((task,error)=>{

		if(error){
			console.log(error);
			return false;
		}else{
			return task;
		}
	})
}

//Controller function for deleting a task
//"taskId" is the URL parameter passed from the "taskRoute.js" file
//Business Logic
/*
	1. Look for the task with the corresponding id provided in the URL/route
	2. Delete the task using the Mongoose method "findByIdAndRemove" with the same id provided in the route
*/

module.exports.deleteTask = (taskId) =>{
	return Task.findByIdAndRemove(taskId).then((removedTask,err)=>{

		if(err){
			console.log(err);
			return false;
		} else {
			return removedTask
		}
	})
}

//Controller function for updating a task
//Business Logic
/*
	1. Get the task with id using the Mongoose method "findById"
	2. Replace the task's name returned from the db with the "name" property from the request body
	3. Save task
*/

module.exports.updateTask = (taskId, newContent)=>{

	return Task.findById(taskId).then((result,err)=>{
		if(err){
			console.log(err);
			return false;
		}

		result.name = newContent.name;

		return result.save().then((updatedTask,saveErr)=>{

			if(saveErr){
				console.log(saveErr);
				return false;
			} else {
				return updatedTask;
			}
		})
	})
}

//Activity s42
module.exports.getTask = (taskId) =>{
	return Task.findById(taskId).then((result,err)=>{
		if(err){
			console.log(err);
			return false;
		} else {
			return result;
		}
	})
}

module.exports.completeTask = (taskId) => {
  return Task.findByIdAndUpdate(taskId, { status: "Complete" }).then(
    (result, error) => {
      if (error) {
        console.log(error);
        return false;
      } else {
        return Task.findById(taskId).then((updatedTask, saveErr) => {
          if (saveErr) {
            console.log(saveErr);
            return false;
          } else {
            return updatedTask;
          }
        });
      }
    }
  );
};