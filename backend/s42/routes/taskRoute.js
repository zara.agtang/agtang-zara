//Contain all the endpoints for our application

const express = require("express");
//create a Router instance that funcitons as a middleware and routing system
const router = express.Router()

//use the functions inside taskController.js
const taskController = require("../controllers/taskController");

//[Routes]
//routes are responsible for defining the URIs that our client accesses and the corresponding controller functions that will be used when a route is accessed
//All the business logic is done in the controller

router.get("/",(req,res)=>{

	taskController.getAllTasks().then(resultFromController=>res.send(resultFromController))
})

router.post("/",(req,res)=>{

	taskController.createTask(req.body).then(resultFromController=>res.send(resultFromController))
})

//Route to delete a task
//This route expects to receive a DELETE request at the "/tasks/:id"
//http://localhost:4000/tasks/:id
//task id is obtained from the URL is denoted by ":id" identifier in the route

router.delete("/:id",(req,res)=>{
	taskController.deleteTask(req.params.id).then(resultFromController=>res.send(resultFromController))
})

//Route to update a task
//This route expects to receive a PUT request at the "/tasks/:id"
//http://localhost:4000/tasks/:id

router.put("/:id",(req,res)=>{
	taskController.updateTask(req.params.id, req.body).then(resultFromController=>res.send(resultFromController))
})

//Activity s42
router.get("/:id",(req,res)=>{

	taskController.getTask(req.params.id).then(resultFromController=>res.send(resultFromController))
})

router.put("/:id/complete", (req, res) => {
  taskController
    .completeTask(req.params.id)
    .then((resultFromController) => res.send(resultFromController));
});


//use module.exports to export the router object to use in index.js
module.exports = router;