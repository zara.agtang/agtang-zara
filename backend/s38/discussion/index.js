let http = require("http");
let port = 4000;

let app = http.createServer((request,response)=>{

	/*
		HTTP requests are differentiated not only via their endpoints but also with their methods
		HTTP methods simpy tells the server what action it must take or what kind of response is needed for our request
		With an HTTP method, we can actually create routes with the same endpoint but with different methods
	*/
	/*
		url: localhost:4000/
		method: GET

		url: localhost:4000/
		method: POST

		The url are not the same because they don't have the same action
	*/

	if (request.url == "/items" && request.method == "GET"){
		response.writeHead(200,{'Content-type':'text/plain'});
		response.end('Data received from the database!')
	}


	if (request.url == "/items" && request.method == "POST"){
		response.writeHead(200,{'Content-type':'text/plain'});
		response.end('Data to be sent to the database!')
	}

})
//The listen method can take 2 arguments
//1. port number to assign our server to
//2. callback method/funciton to run when the server is already running
app.listen(port,()=>console.log(`Server running at localhost: ${port}`))