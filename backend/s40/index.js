/*
	npm init - used to initialize a new package.json file
	package.json - contains info about the project, dependencies, and other settings
	package-lock.json - locks the versions of all installed dependencies to its specific version
*/

const express = require("express")
const app = express();
const port = 4000;

//Middlewares
	//a software that provides common services and capabilities to application outside of what's offered by operating system
//app.use() is a method used to run another function or method for our expressjs api. It is used to run middlewares

//allows our app to read json data
app.use(express.json())//used to parse the incoming data

//allows our application to read data from forms. This also allows us to receive information in other data types such as an object which we will use throughout our application
app.use(express.urlencoded({extended:true}))//allows us to parse

app.get("/",(req,res)=>{
	res.send("Hello World")
})

app.get("/hello",(req,res)=>{
	res.send("Hello World from /hello endpoint")
})

app.post("/hello",(req,res)=>{
	res.send("Hello World from the post route")
})

//MA1
	//Refactor the post route to receive data from the req body
	//Send a message that has the data received
		
		//req.body.?
		//`Hello firstName lastName`


app.post("/data",(req,res)=>{
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}`)
})


//MA2

app.put('/',(req,res)=>{
	res.send("Hello from a put method route!")
})

app.delete('/',(req,res)=>{
	res.send("Hello from a delete method route!")
})

//This array will store objects when the '/signup' route is accessed
let users = []


/*app.post("/signup",(req,res)=>{

	console.log(req.body)

	users.push(req.body)
	res.send(users)

})*/


//MA3
	//refactor the signup route
		//If contents of the req.body with the property of username and password is not an empty string, this will store the user object via Postman to the users array
			//send a response back to the client/Postman after the request has been processed
		//Else, if the username and password are not complete, an error message will be sent back to the client/Postman
			//please input both username and password


app.post("/signup",(req,res)=>{

	console.log(req.body)

	if(req.body.username !== '' && req.body.password !== ''){
		users.push(req.body)
		res.send(`User ${req.body.username} has been successfully registered`)
	}
	else {
		res.send(`Please input BOTH username and password`)
	}

})


app.put("/change-password",(req,res)=>{

	let message;

	for(let i=0; i<users.length; i++){

		if(req.body.username == users[i].username){

			users[i].password = req.body.password;

			message = `User ${req.body.username}'s password has been updated!`

			console.log(users);

			break;
		}else {
			message = "User does not exist"
		}

	}

	res.send(message);

})



//Activity





//End of Activity


if(require.main === module){
	app.listen(port,()=>console.log(`Server running at port ${port}`))
}

module.exports = app;