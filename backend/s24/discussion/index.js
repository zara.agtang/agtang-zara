console.log("JavaScript Loops!");

/*
	MA
	1. Create a function named greeting() and display the message you want to say to yourself using console.log inside of the function
	2. Invoke the greeting() function 5 times
	3. Take a screenshot
*/

function greeting(){
	console.log("You can do it!");
}

/*greeting();
greeting();
greeting();
greeting();
greeting();
*/

let countNum = 5;//starting value

while(countNum !== 0){
	console.log("This is printed inside the sample loop: " + countNum);
	greeting()
	countNum--;//from 50 to 1 (decreasing value)
}


//[While Loop]
//takes in an expression/condition
//expressions are any unit of code that can be evaluated to a value
//If the condition evaluates to true, the statements inside the code will be executed
//A loop will iterate to a certain number of times until an expression or condition is met
//Iteration - is the term given to the repetition of statements

/*
	Syntax:

	while(expression/condition){
		statement
	}
*/

let count = 5;

//While the value of count is not equal to 0
while(count !== 0){

	//The current vvalue of count is printed out
	console.log("While: " + count)
	//Decreases the value of count by 1 every iteration to stop the loop when it reaches 0
	count--;
}


//[Do/While Loop]

/*
	-this works a lot like the while loop
	-but unlike the while loops, do-while loops guarantee that the code will be executed
	-Syntax:

	do {
		statement
	}while(expression/condition)

*/

let number = Number(prompt("Give me a number: "));

do {
	console.log("Do While: " + number);
	//+= addition assignment operator
	//increase the value of number by 1 after every iteration to stop the loop when it reaches 10 or greater
	//number = number + 1
	number += 1;

}while(number < 10)


//[For Loop]

/*
	A for loop is more flexible than while and do-while loops
	It consists of three parts:
	1. Initialization - value that will track the progression of the loop
	2. Expression/Condition - that will be evaluated which will determine whether the loop will run one more time
	3. finalExpression - indicates how to advance the loop

	Syntax:

		for (initialization; expression/condition; finalExpression){
			statement
		}
*/

/*
	- Will create a loop that will start from 0 and end at 20
	- Every iteration of the loop, the value of count will be checked if it is equal or less than 20
	- If the value of count is less than or equal to 20, the statement inside of the loop will execute
	- The value of count will be incremented by one for each iteration
*/

	for (let count = 0; count <= 20; count++){
		console.log("For: " + count);
	}


//[Strings]

	let myString = "Taylor Swift"
	//characters in strings may be counted using the .length property
	console.log(myString.length);//12 characters

	//accessing elements of a string
	console.log(myString[0]);//T
	console.log(myString[1]);//a
	console.log(myString[2]);//y

	//will create a loop that will print out the individual letters of the myString variable
	for(let x = 0; x < myString.length; x++){
		console.log(myString[x])
	}

//Create a string named "myName" with a value of your name

	let myName = "Bianca";

	/*
		Create a loop that will print out the letters of the name individually and print out the number 3 instead when the letter to be printed out is a vowel
	*/

	for(let i=0; i<myName.length; i++){

		//console.log(myName[i].toLowerCase());

		if(
			myName[i].toLowerCase() == "a" ||
			myName[i].toLowerCase() == "e" ||
			myName[i].toLowerCase() == "i" ||
			myName[i].toLowerCase() == "o" ||
			myName[i].toLowerCase() == "u"
		){
			console.log(3);
		}

		else{
			console.log(myName[i]);
		}

	}

//[Continue and Break Statements]
/*
	- continue statement allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block
	- break statement is used to terminate the current loop once a match has been found
*/

	/*
		create a loop that if the count value is divisible by 2 and the remainder is 0, it will print the number and continue to the next iteration of the loop
	*/

for (let count = 0; count <= 20; count++){

	if(count % 2 === 0){
		//console.log("Continue and Break: " + count);//will print out even numbers
		continue;
	}
	console.log("Continue and Break: " + count);//prints out odd numbers

	if(count>10){
		break;
	}
}

/*
	MA

	- Create a loop that will iterate based on the length of the string
	1.Initialize the variable name with the value "Bernardo". The loop will start at 0 for the value of "i"
	2. It will check if "i" is less than the length of name (e.g. 0), the number i will increase every iteration
	3. The if statement will check if the value of name[i] converted to a lowercase letter a (e.g. name[0] = a)
	4. If the expression/condition of the if statement is true the loop will continue to the next iteration.
	5. If the value of name[i] is not equal to a, the second if statement will be evaluated
	6. The second if statement will check if the value of name[i] converted to a lowercase letter d (e.g. name[0] = d)
	7. If the expression/condition of the second "if" statement is false the loop will proceed to the next iteration.
	8. Then the loop will repeat steps 2 to 8 until the expression/condition of the loop is "false" or the condition of the second "if" statement (e.g. name[0] = d) is true, the loop will stop due to the "break" statement

*/

/*let name = "Bernardo";

for (let i=0; i<name.length; i++){

	//If the vowel is equal to a, continue to the next iteration of the loop
	if(name[i].toLowerCase() === "a"){

		console.log("Continue to the next iteration");
		continue;

	}

	//The current letter is printed out based on it's index
	console.log(name[i]);

	}
	
	//If the current letter is equal to d, stop the loop
	if (name[i].toLowerCase === "d"){
		break;
	}*/