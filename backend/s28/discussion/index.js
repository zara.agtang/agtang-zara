console.log("Array Non-Mutator and Iterator Methods");

/*
	Non-Mutator Methods
		- Non-Mutator methods are functions that do not modify or change an array after they are created
		- Do not manipulate the original array performing various tasks such as:
			- returning elements from an array
			- combining arrays
			- printing the output
*/

let countries = ['US','PH','CAN','SG','TH','PH','FR','DE'];

//indexOf()
/*
	- Returns the index number of the first matching element found in an array
	- If no match is found, the result will be -1
*/

	let firstIndex = countries.indexOf('PH');
	console.log('Result of indexOf method: ' + firstIndex);//1

	let invalidCountry = countries.indexOf('BR');
	console.log('Result of indexOf method: ' + invalidCountry);//-1


//lasIndexOf()
/*
	Returns the index number of the last matching element found in the array
*/

	let lastIndex = countries.lastIndexOf('PH');
	console.log('Result of lastIndexOf method: ' + lastIndex); //5

	let lastIndexStart = countries.lastIndexOf('PH', 6); //5
	//let lastIndexStart = countries.lastIndexOf('PH', 3); //1
	console.log('Result of lastIndexOf method: ' + lastIndexStart);


//slice()
/*
	Portions/slices elements from an array AND returns a new array
*/

console.log(countries); //['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE']

let slicedArrayA = countries.slice(2); //sliced from index number 2
console.log('Result from slice method:');
console.log(slicedArrayA);//['CAN', 'SG', 'TH', 'PH', 'FR', 'DE']

let slicedArrayB = countries.slice(2,4); //sliced from 2-4
console.log('Result from slice method:');
console.log(slicedArrayB);//['CAN', 'SG']

let slicedArrayC = countries.slice(-3); //sliced the last 3 elements from the array
console.log('Result from slice method:');
console.log(slicedArrayC);//['PH', 'FR', 'DE']


//toString()
/*
	Returns an array as a string separated by commas
*/

let stringArray = countries.toString();
console.log('Result from toString method:');
console.log(stringArray);

let sampArr = [1,2,3];
console.log(sampArr.toString());


//concat()
/*
	Combines two arrays and returns the combined result
*/

let taskArrayA = ['drink html','eat javascript'];
let taskArrayB = ['inhale css','breathe mongodb'];
let taskArrayC = ['get git','be node'];

let tasks = taskArrayA.concat(taskArrayB);
console.log('Result from concat method:');
console.log(tasks);

console.log('Result from concat method:');
let allTasks = taskArrayA.concat(taskArrayB,taskArrayC);
console.log(allTasks);

//you can also add other elements that are not in the orig arrays
let combinedTasks = taskArrayA.concat('smell epress','throw react');
console.log('Result from concat method:');
console.log(combinedTasks);


//join()
/*
	Returns an array as a string separated by specified separator (string data type)
*/

let users = ['John','Jane','Joe','James'];
console.log(users.join());//default: John,Jane,Joe,James
console.log(users.join(''));//JohnJaneJoeJames
console.log(users.join(' - '));//John - Jane - Joe - James


//Iteration Methods
/*
	- These are loops designed to perform repetitive tasks on arrays
	- Array Iteration methods normally work with a function suppllied as an argument
*/

//forEach()
/*
	- Similar to a for loop that iterates on each array element
	- For each item in the array, the ANONYMOUS FUNCTION passed in the forEach() method will be run
	- The anonymous function is able to receive the current item being iterated or loop over by assigning a PARAMETER
	- Variable names for arrays are normally written in the plural form
	- It is a common practice to use singular form of the array content for the parameter names used in array loops
	- forEach() does not return anything
*/

console.log(allTasks);//['drink html', 'eat javascript', 'inhale css', 'breathe mongodb', 'get git', 'be node']
	
	//task is a parameter that is singular form of the array
	//task represents each array elements
	allTasks.forEach(function(task){
		console.log(task);
	});

	let filteredTasks = [];

	allTasks.forEach(function(task){
		
		//console.log(task);
		if(task.length > 10){

			filteredTasks.push(task);
		}
	});

	console.log("Result of filteredTasks:");
	console.log(filteredTasks);//['eat javascript', 'breathe mongodb'] -  arrays that has greater than 10 characters



//map()
/*
	- Iterates on each element AND returns a new array with different values depending on the result of the function's operation
	- It requires the use of a "return"
*/

	let numbers = [ 1, 2, 3, 4, 5 ];

	let numberMap = numbers.map(function(number){
		return number * number;
	})

	console.log('Original Array:');
	console.log(numbers);
	console.log('Result of map method:');
	console.log(numberMap);

	//map() vs forEach()

	let numberForEach = numbers.forEach(function(number){
		return number * number;
	})

	console.log(numberForEach);//undefined


//every()
/*
	- Checks if all elements in an array meet a given condition
	- This returns a BOOLEAN value
*/

let allValid = numbers.every(function(number){
	return (number < 3);
	//return (are all numbers less than 3)
})

console.log("Result of every method:");
console.log(allValid);//FALSE because not EVERY numbers in numbers array are not less than 3


//some()
/*
	Checks if at least one element in the array meets the given condition
*/

let someValid = numbers.some(function(number){
	return (number < 2);
	//return (are some numbers less than 2?)
})

console.log("Result of some method:");
console.log(someValid);//TRUE because some numbers are less than 2


//filter()
/*
	- Returns a new array that contains elements which meets the given condition
	- Returns an empty array if no elements were found
*/

let filterValid = numbers.filter(function(number){
	return (number < 3);
	//return numbers that are less than 3
})

console.log("Result of filter method:");
console.log(filterValid);//[1,2]

let nothingFound = numbers.filter(function(number){
	return (number = 0);
	//return numbers that are equal to zero
})

console.log("Result of filter method:");
console.log(nothingFound);//[]


//MINI ACTIVITY

/*Filter out numbers from the numbers array that are less than 3 using forEach.*/

let filteredNumbers = [];

numbers.forEach(function(number){
	if(number < 3){
		filteredNumbers.push(number);
	}
})

console.log("Result of forEach method:");
console.log(filteredNumbers);


let products = ['Mouse','Keyboard','Laptop','Monitor'];

//includes()
/*
	- Checks if the argument passed can be found in the array
	- Returns a BOOLEAN value
*/

let productFound1 = products.includes("Mouse");
console.log(productFound1);//true

let productFound2 = products.includes("Headset");
console.log(productFound2);//false



//METHOD CHAINING
/*
	Methods can be "chained" using them one after another
*/

let filteredProducts = products.filter(function(product){

	return product.toLowerCase().includes('a');
})

console.log(filteredProducts);//['Keyboard', 'Laptop']


//reduce()
/*
	Evaluates elements from left to right and returns/reduces the array into a single value

	//"accumulator" parameter in the function stores the result for every iteration of the loop
	//"current value" parameter is the current element in the array that is evaluated in each iteration of the loop

	Process:
	1. first/result element in the array is stored in the "accumulator"
	2. second/next element in the array is stored in the current value
	3. an operation is performed on the 2 elements
	4. the loop repeats the whole step 1-3 until all elements have been worked on 
*/

console.log(numbers);//[1, 2, 3, 4, 5]
					//1 + 2 = 3
					//3 + 3 = 6
					//6 + 4 = 10
					//10 + 5 = 15

let iteration = 0;

let reducedArray = numbers.reduce(function(x,y){
	//use to track the current iteration count and accumulator/currentValue data
	console.warn('Current iteration: ' + ++iteration);
	console.log('Accumulator: ' + x);
	console.log('Current Value: ' + y);
	return x + y;
})

console.log("Result of reduce method: " + reducedArray);
console.log(reducedArray);


//reduce string arrays to string

let list = ['Hello', 'From', 'The', 'Other', 'Side'];
let reducedJoin = list.reduce(function(x,y){
	console.log(x);
	console.log(y);
	return x + ' ' + y;
})

console.log("Result of reduce method: " + reducedJoin);