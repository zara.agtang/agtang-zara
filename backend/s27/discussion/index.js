console.log("Array Manipulation!");

//Array Methods
	//JS has built-in functions and methods for arrays. This allows us to manipulate and access array items.

	//Mutator Methods
	/*
		- are functions that mutate or change an array after they are created
		- these methods manipulate the original array performing various tasks such as adding and removing elements
	*/

	//array
	let fruits = ['Apple','Orange','Kiwi','Dragon Fruit'];
	console.log(fruits);
	console.log(typeof fruits);

//push()
//Adds an element in the END of an array AND returns the array's LENGTH
//Syntax:
	//arrayName.puh("itemToPush");

	console.log('Current array:');
	console.log(fruits);

	//if we assign to a variable, we are able to save the length of the array

	let fruitsLength = fruits.push('Mango');
	console.log(fruitsLength);
	console.log('Mutated array from push method:');
	console.log(fruits);
	//we can push multiple items at the same time
	//fruits.push('samplefruit','samplefruit2');
	//console.log(fruits);

//pop()
//Removes the LAST element in an array AND returns the removed element

//Syntax
	//arrayName.pop();

	let removedFruit = fruits.pop();
	console.log(removedFruit);
	console.log('Mutated array from pop method:');
	console.log(fruits);
	//we cannot delete multiple elements with pop

	//MA
		//Create a function called removeFruit()
		//deletes the last item in the array
		//after invoking the removeFruit function. Log the fruits in the array console.

	/*function removeFruit(arr){
		arr.pop();
	}

	removeFruit(fruits);
	console.log(fruits);*/

	//or

	function removeFruit(){
		fruits.pop();
	}

	removeFruit();
	console.log(fruits);//deleted dragon fruit

//unshift()
//adds one or more elements at the beginning of an array
//Syntax
	// - arrayName.unshift('elementA');
	// - arrayName.unshift('elementA','elementB');

	fruits.unshift('Lime','Banana');//added lime and banana before the existing array
	console.log('Mutated array from unshift method:');
	console.log(fruits);

	//add 2 more before the existing array
	function unshiftFruit(fruit1, fruit2){
		fruits.unshift(fruit1, fruit2);
	}

	unshiftFruit("Grapes","Papaya");
	console.log(fruits);

//shift()
//removes an element at the beginning of an array AND returns the removed element
//Syntax
	//arrayName.shift();

	let anotherFruit = fruits.shift();
	console.log(anotherFruit);//
	console.log('Mutated array from the shift method:');
	console.log(fruits);

//splice()
//simultaneously removes elements from a specified index number and adds elements
//syntax
	//arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);

	fruits.splice(1, 2,'Calamansi','Cherry');//replaced Lime and Banana

	console.log('Mutated array from splice method:');
	console.log(fruits);

	function spliceFruit(index,deleteCount,fruit){
		fruits.splice(index,deleteCount,fruit);
	}

	spliceFruit(1,1,"Avocado");//replaced Calamansi
	spliceFruit(2,1,"Durian");//replaced Cherry
	//spliceFruit(2,1);//this will result to undefined
	console.log(fruits);

//sort()
//this rearranges the array elements in alphanumeric order (A-Z)
//syntax
	//arrayName.sort()

	fruits.sort();
	console.log('Mutated array from sort method:');
	console.log(fruits);

//reverse()
//this reverses the order of array elements(Z-A)
//syntax
	//arrayName.reverse();

	fruits.reverse();
	console.log('Mutated array from reverse method:');
	console.log(fruits);