//MongoDB Query Operators and Field Projection

//[Comparison Query Operators]

//Greater than/Greater than or equal
//$gt/$gte operator
/*
	Allows us to find documents that have field number values greater than or equal to a specified value
	Syntax:
		db.collectionName.find({field: {$gt: value}})
		db.collectionName.find({field: {$gte: value}})
*/

	db.users.find({age:{$gt:50}})
	db.users.find({age:{$gte:50}})

//Lesser than/Lesser than or equal
//$lt/$lte operator
/*
	Allows us to find documents that have field number values less than or equal to a specified value
	Syntax:
		db.collectionName.find({field: {$lt: value}})
		db.collectionName.find({field: {$lte: value}})
*/

	db.users.find({age:{$lt:50}})
	db.users.find({age:{$lte:50}})


//$ne operator

/*
	Allows us to fing documents that have field number values not equal to a specified value
	Syntax:
		db.collectionName.find({field:{$ne : value}})
*/

	db.users.find({age: {$ne: 65}})
	db.users.find({age: {$ne: 82}})

//$in operator
/*
	Allows us to find documents with specific match criteria in one field using different values
	Syntax:
		db.collectionName.find({field: {$in: [<value>,<value>,...]}})
*/

	db.users.find({lastName: {$in: ["Hawking","Doe"]}})
	db.users.find({courses:{$in: ["HTML","React"]}})

//[Logical Query Operators]

//$or
/*
	Allows us to find documents that match a single criteria from multiple provided search criteria

	Syntax:
		db.collectionName.find({$or: [{fieldA: valueA},{fieldB: valueB}]})
*/
	db.users.find({$or:[{firstName:"Neil"},{age:25}]})
	db.users.find({$or:[ {firstName:"Neil"}, {age: {$gt:30} } ] })

//$and
/*
	Allows us to find documents matching multiple criteria in a single field
	Syntax:
		db.collectionName.find({$and: [{fieldA: valueA},{fieldB: valueB}]})
*/
	db.users.find({$and: [{age:{$ne:82}},{age:{$ne:76}}]})


//[Field Projection]
/*
	Retrieving documents are common operations that we do and by default MongoDB queries return the whole document as a response

	We can now include or exclude fields from the response
*/

//Inclusion
	/*
		Allows us to include/add specific fields only when retrieving documents
			- the value provided is 1 to denote that the field is being included
		Syntax:
		db.users.find({criteria},{field: 1})
	*/

	db.users.find(
		{firstName: "Jane"},
		{
			firstName: 1,
			lastName: 1,
			contact: 1
		}
	)

//Exclusion
	/*
		Allows us to exclude/remove specific fields only when retrieving documents
		- the value provided is 0 to denote that the field is being excluded
		Syntax:
			db.users.find({criteria},{field:0})
	*/

	db.users.find(
		{firstName: "Jane"},
		{
			contact: 0,
			department:0
		}
	)

//Suppressing the ID Field
/*
	Allows us to exclude the "_id" field when retrieving documents
	When using field projection, field inclusion and exclusion may not be used at the same time
	Excluding the "_id" field is the only exception to this rule
	Syntax:
		db.users.find({criteria},{_id:0})
*/

	db.users.find(
		{
			firstName: "Jane"
		},
		{
			firstName: 1,
			lastName: 1,
			contact: 1,
			_id: 0
		}
	)

//[Evaluation Query Operator]

	//$regex operator
	/*
		-Allows us to find documents that match a specific string pattern using regular expressions

		Syntax:
			db.users.find({field: {$regex: 'pattern', $options: 'optionvalue'} })
	*/

	//Case sensitive query
	db.users.find({firstName: {$regex: 'N'}})

	//Case insensitive query
	//The 'i' is for insensitive
	db.users.find({firstName: {$regex: 'j', $options: 'i'}})