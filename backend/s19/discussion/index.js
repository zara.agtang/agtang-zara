// Syntax, Statements, and Comments


console.log("We tell the computer to log this in the console.");
alert("We tell the computer to display an alert with this message!");

// Statements - in programming are instructions that we tell the computer to perform
// JS Statements usually ends with a semicolon (;)
// Semicolons are not required in JS, but we will use it to help us train to locate where a statement ends

// Syntax - it is the set of rules that describes how statements must be constructed
// All lines/blocks of code should be written in a specific manner to work. This is due to how these codes were initially programmed to function and perform in a certain manner

// Comments
// For us to create a comment, we use Ctrl + /
// We have single line (ctrl + /) and multi-line comments (ctrl + shift + /)

// console.log("Hello!");

/*alert("This is an alert!");
alert("This is another alert");*/

//We can also type a simple statement in dev tools
//For all operating systems: Right click + Inspect
//Linux and Windows: Ctrl + Shift + I
//Mac: Option + Command + J
//we used devtools not just to manipulate css but also allowing you to DEBUG, view messages and run JavaScript code in the console tab.

// Whitespaces (spaces and line breaks) can impact functionality in many computer languages, but not in JS.
//In JavaScript, whitespace is used only for readability and has no functional impact.  One effect of this is that a single statement can span multiple lines.


	// What is the use of let and const?
		// declare a variable, const is for constant, let does not need to be constant
	// What else do we need to declare a variable

		// let/const variableName;

	// What do we call the value "Camille"
		// let firstName = "Camille"; (initialValue)
	
	// What do we call this:
		// firstName = "Cee"; (re-assignment)


// Variables (it is like a box or container)
// This is used to contain data
// Any information that is used by an application is stored in what we call a "memory"
// When we create variables, certain portions of a device's memory is given a "name" that we call "variables"
// This makes it easier for us associate information stored in our devices to actual "names" about information


// Declare variables
	// tell our devices that a variableName is created and is ready to store data
	// Syntax
		// let/const variableName;

	let myVariable;
	console.log(myVariable);//undefined

	// Variables must be declared first before they are used
	// using variables before they are declared will return an error
	let hello;
	console.log(hello);

	/*
	    Guides in writing variables:
	        1. Use the 'let' keyword followed by the variable name of your choosing and use the assignment operator (=) to assign a value.
	        	create a variable named firstName with the value of your first name;

	        2. Variable names should start with a lowercase character, use camelCase for multiple words.

				let name;

				let firstName;

				let myFirstExample;

	        3. For constant variables, use the 'const' keyword.
				
				const pi;

				const interestRate;

	        4. Variable names should be indicative (or descriptive) of the value being stored to avoid confusion.


	        	let a = "The sentence is correct."

	        	let pokemon = "Pikachu";

	    Best practices in naming variables:

			1. When naming variables, it is important to create variables that are descriptive and indicative of the data it contains.

				let firstName = "Michael"; - good variable name
				let pokemon = 25000; - bad variable name

			2. When naming variables, it is better to start with a lowercase letter. We usually avoid creating variable names that starts with capital letters. Because there are several keywords in JS that start in capital letter.

				let FirstName = "Michael"; - bad variable name
				let firstName = "Michael"; - good variable name

			3. Do not add spaces to your variable names. Use camelCase for multiple words, or underscores.

				let first name = "Mike";

			camelCase is when we have first word in small caps and the next word added without space but is capitalized:

				lastName emailAddress mobileNumber

			Underscores sample:

			let product_description = "lorem ipsum"
			let product_id = "250000ea1000"

	*/


	// declaring and initializing variables
	// Syntax
		// let/const variableName = initialValue;

	let productName = 'desktop computer';
	console.log(productName);

	let productPrice = 18999;
	console.log(productPrice);

	// const - values that should not change
	const interest = 3.539;

	// re-assigning variable values

	productName = 'Laptop';
	console.log(productName);

	// we cannot replace constant values
	// interest = 3.5;
	// console.log(interest);

	// let variable cannot be re-declared within its scope
	let friend = 'Kate';
	friend = 'Jane';

	// Reassigning variables vs initializing variables

	// Declare a variable
	let supplier;

	// Initialization is done after the variable has been declared
	supplier = "John Smith Tradings";

	// This is considered as re-assignment because its initial value was already declared
	supplier = "Zuitt Store";

	// We cannot declare a const variable without initialization.
	/*const pi;
	pi = 3.1416;
	console.log(pi);*/

	// it should be like this
	/*const pi = 3.1416;
	console.log(pi);*/

	// Multiple variable declarations

	let productCode = 'DC017', productBrand = 'Dell';

	// let productCode = 'DC017';
	// const productBrand = 'Dell';
	console.log(productCode, productBrand);

	// Data Types

	// Strings
	//" " or ' ' (String Literals)
	// Strings are a series of characters that creates a word, a phrase or a sentence, or anything related to creating text
	// Strings in JavaScript can be written using either a single (') or double (") quote
	// In other programming languages, only the double quotes can be used for creating strings


	let country = 'Philippines';
	let province = "Metro Manila";

	// Concatenating Strings
	// Multiple string values can be combined to create a single string using the "+" symbol

	let fullAddress = province + ', ' + country;
	console.log(fullAddress);

	let greeting = 'I live in the ' + country;
	console.log(greeting);

	// The escape character (\) in strings in combination with other characters can produce different effects

	// "\n" refers to creating a new line in between text

	let mailAddress = 'Metro Manila\n\n\n\nPhilippines';
	console.log(mailAddress);

	let message = "John's employees went home early";
	console.log(message);
	message = 'John\'s employees went home early';
	console.log(message);

	// Numbers

	let headcount = 26;
	console.log(headcount);
	let grade = 98.7;
	console.log(grade);
	let planetDistance = 2e10;
	console.log(planetDistance);

	// Combine text and strings
	console.log("John's first grade last quarter is " + grade);

	// Boolean
	let isMarried = false;
	let isGoodConduct = true;
	console.log(isMarried);

	console.log("isGoodConduct: " + isGoodConduct);

	// Arrays

	//[] (square brackets - Array Literals)

	// Arrays are a special kind of data type that's used to store multiple values
	// Arrays can store different data types but is normally used to store similar data types
	//Best practice is to store elements with similar data types

	// Syntax
		// let/const arrayName = [elementA, elementB, ...]
	let grades = [98.7,92.1,90.7,98.6];
	console.log(grades);

	// different data types
	// storing different data types inside an array is not recommended because it will not make sense to in the context of programming
	let details = ["John", "Smith", 32, true];
	console.log(details);


	// Objects
	//Objects
	// Objects are another special kind of data type that's used to mimic real world objects/items
	// They're used to create complex data that contains pieces of information that are relevant to each other
	// Every individual piece of information is called a property of the object

	// Syntax
	    // let/const objectName = {
	    //     propertyA: value,
	    //     propertyB: value,
	    // }

	// They're also useful for creating abstract objects

	let myGrades = {
		firstGrading: 98.7,
		secondGrading: 92.1,
		thridGrading: 90.7,
		fourthGrading: 98.6,
	}
	console.log(myGrades);

	// You can add Array inside an Object
	let person = {

		fullName: 'Juan Dela Cruz',
		age: 35,
		isMarried: false,
		contact: ["+639123456789","87000"],
		address:{
			houseNumber: '345',
			city:'Manila'
		}

	}
console.log(person);

// typeof operator
//typeof operator is used to determine the type of data or the value of a variable. It outputs a string.

console.log(typeof person);//object

//Note: Array is a special type of object with methods and functions to manipulate it. We will discuss these methods in later sessions. (Javascript - Array Manipulation)
console.log(typeof grades);



// Null
	// it is used to intentionally express the absence of the value in a variable declaration/initialization

	let spouse = null;

// Undefined - no initial value (it is a data type)
	// this represents the state of a variable that has been declared but without an assigned value

	let fullName;
	console.log(fullName);//undefined


	let dog = "Happy";
	console.log(dog);


	let message = "Hi, My name is Camille.";
	console.log(message);