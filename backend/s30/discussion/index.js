console.log("ES6 UPDATES!");

//ES6 Updates
	//ES6 is one of the latest versions of writing JS and in fact ONE of the MAJOR updates

	//let and const
		//are ES6 updates, these are the new standards of creating variables

	//In JS, hoisting allows us to use fucntions and variables before they are declared
	//BUT this might cause confusion, because of the confusion that var hoisting can create, it is BEST to AVOID using Variables before they are declared

	console.log(varSample);
	var varSample = "Hoist me up!!";

	//if you have used nameVar in other parts of the code, you might be surprised at the output we might get

	var nameVar = "Bianca";

	if(true){
		var nameVar = "B"
	}

	console.log(nameVar);

	let name1 = "Bee";

	if (true){
		let name1 = "Hello";
	}

	//let name1 = "Hello World";

	console.log(name1);

//[** Exponent Operator]

	//Update
	const firstNum = 8**2;
	console.log(firstNum);//64

	const secondNum =  Math.pow(8,2);
	console.log(secondNum);

let string1 = 'fun';
let string2 = 'Bootcamp';
let string3 = 'Coding';
let string4 = 'JavaScript';
let string5 = 'Zuitt';
let string6 = 'love';
let string7 = 'Learning';
let string8 = 'I';
let string9 = 'is';
let string10 = 'in';

/*
	MA1
	1. Create a new variable called concatSentence1
	2. Concatenate and save a resulting string into concatSentence1
	3. Log the concatSentence1 in your console and take a screenshot
	**the sentences MUST have spaces and punctuation
*/

//[Template Literals `${}`]
/*
	Allows us to write strings withour using the concatenation operator (+)
	Greatly helps with code readability
*/

let concatSentence1 = `${string8} ${string6} ${string5} ${string3} ${string2}!`;
console.log(concatSentence1);

let concatSentence2 = `${string3} ${string9} ${string1}!`;
console.log(concatSentence2);

/*
	concat method for strings
	let sentence = concatSentence1.concat(". ", concatSentence2);
	console.log(sentence);
*/

//${} is a placeholder that is used to embed JavaScript expressions when creating strings using template literals

let name = "Carding";
let message = `Hello ${name}! Welcome to programming!`;
console.log(`Message with template literals: ${message}`);

//multi-lines using template literals
const anotherMessage = `

	${name} attended a math competition.

	He won it by solving the problem 8**2 with the solution of ${firstNum}!
`
console.log(anotherMessage);

const interestRate = .1;
const principal = 1000;
console.log(`The interest on your savinga account is: ${principal * interestRate}`);


let dev = {
	name: "Peter",
	lastName: "Parker",
	occupation: "Web Developer",
	income: 50000,
	expenses: 60000
};

console.log(`${dev.name} is a ${dev.occupation}.`);
console.log(`His income is ${dev.income} and expenses at ${dev.expenses}. His current balance is ${dev.income - dev.expenses}.`);

//Array Destructuring
/*
	- Allows us to unpack elements in arrays into distinct variables.
	- Allows us to name array elements with variables instead of using index numbers.
	- Helps with code readability.

	Syntax:
		let/const [variableName, variableName, variableName] = array;
*/

const fullName = ["Juan","Dela","Cruz"];
//Pre-Array Destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);
console.log(`Hello, ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you!`);

//Array Destructuring

//if you want to log just the fName and lName, leave the mName blank
const [firstName, middleName, lastName] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);
console.log(`Hello, ${firstName} ${middleName} ${lastName}! I was enchanted to meet you!`);
console.log(fullName);

let fruits = ["Mango","Grapes","Guava","Apple"];
let [fruit1, , fruit3, ] = fruits;
console.log(fruit1);
console.log(fruit3);


//MA2
	//destructure this array and log the individual variables
	//stretch goal, do not assign a variable name for Taguro
let kupunanNiEugene = ["Eugene","Alfred","Vincent","Dennis","Taguro","Master Jeremiah"];
let [m1, m2, m3, m4, , m5] = kupunanNiEugene;
console.log(m1);
console.log(m2);
console.log(m3);
console.log(m4);
console.log(m5);

//[Object Destructuring]
/*
	- Allows us to unpack properties of objects into distinct variables
	- Shortens the syntax for accessing properties from objects
	- You should always use the propertyName in destructuring

	Syntax:
		let/const {propertyName, propertyName, propertyName} = object;
*/

const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
}

//pre-object destructuring
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);
console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again!`);

//Object Destructuring
const { givenName, maidenName, familyName} = person;

console.log(givenName);
console.log(maidenName);
console.log(familyName);

console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again!`);

function getFullName ({givenName, maidenName, familyName}){
	console.log(`${givenName} ${maidenName} ${familyName}`)
}

getFullName(person);


//[Arrow functions]

/*
	- Compact alternative syntax to traditional functions
	- Useful for code snippets where creating functions will not be reused in any other portion of the code
	- Adhere to "DRY" (Don't Repeat Yourself) principle where there's no longer need to create a function and think of a name for functions that will only be used in certain code snippets
*/

	//traditional function sample
	function displayMsg(){
		console.log("Hi");
	}

	displayMsg();

	//arrow function sample
	let displayHello = () => {
		console.log("Hello World!")
	}

	displayHello();

	//traditional function with parameters
	/*function printFullName (firstName,middleInitial,lastName){
		console.log(`${firstName} ${middleInitial} ${lastName}`);
	}

	printFullName("John","D","Smith");*/

	let printFullName = (firstName,middleInitial,lastName) =>{
		console.log(`${firstName} ${middleInitial} ${lastName}`)
	}

	printFullName('John','D','Smith');


	//another sample
	const students = ["John","Jane","Natalia","Jobert","Joe"];

	//arrow function with loops

	//traditional (pre-arrow)
	students.forEach(function(student){
		console.log(`${student} is a student!`);
	})


	//arrow
	students.forEach((student)=>{
		console.log(`${student} is a student. (from arrow)`)
	})


//[Implicit Return Statement]
/*
	- There are instances when you can omit the "return" statement
	- This works because even without the "return" statement JS implicitly adds it for the result of the function
*/
	//traditional
	/*function add (x,y){
		return x + y;
	}

	let total = add(1,2);
	console.log(total);//3*/


	//arrow
	/*const add = (x,y) => {
		return x + y;
	}
	
	let total = add(1,2);
	console.log(total);//3
	*/


	//or you can use this one-liner
	const add = (x,y) => x + y;
	let total = add(1,2);
	console.log(total);//3


//[Default Function Argument Value]
	//"User" is the defaulr function argument value
	const greet = (name = 'User') =>{
		return `Good morning, ${name}!`
	}

	console.log(greet());
	console.log(greet("John"));


//[Class-Based Object Blueprints]
/*
	Allows the creation/instantiation of objects using classes as blueprints
*/

//Creating a class
/*
	- The constructor is a special method of a class for creating/initializing an object for that class
	- "this" keyword refers to the properties of an object created/initialized from the class
*/

	class Car {
		constructor(brand,name,year){
			this.brand = brand;
			this.name = name;
			this.year = year;
		}
	}

	let myCar = new Car();

	console.log(myCar);

	myCar.brand = "Ford";
	myCar.name = "Ranger Raptor";
	myCar.year = 2021;

	console.log(myCar);


	const myNewCar = new Car("Toyota","Vios",2021);
	console.log(myNewCar);


//Traditional functions vs Arrow Function as methods

let character1 = {

	name: "Cloud Strife",
	occupation: "Soldier",
	greet: ()=>{

		//In a traditional function:
			//this keyword refers to the curreent object where the method is
		console.log(this);
		//global window object
			//container of browser's global variables, functions, and objects
		console.log(`Hi! I'm ${this.name}`);
	},
	introduceJob: function(){
		console.log(`Hi! I'm ${this.name}. I'm a ${this.occupation}`);
	}
}

character1.greet();
character1.introduceJob();


/*
	Mini-Activity (3-5mins)
	Create a "Character" class constructor
	name:
	role:
	strength:
	weakness:

	create 1 new instance of your variable character from the class constructor and save it in their variables
	log the variables and send ss
*/

class Character {
	constructor(name,role,strength,weakness) {
		this.name = name;
		this.role = role;
		this.strength = strength;
		this.weakness = weakness;
		this.introduce = () =>{
			console.log(`Hi I am ${this.name}!`)
		}
	}
}

const myCharacter = new Character("Chee","Warrior","n/a","n/a")

console.log(myCharacter);
myCharacter.introduce();