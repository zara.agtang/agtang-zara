//Dependencies and Modules
const express = require("express");
const courseController = require("../controllers/course");
const auth = require("../auth") 
const {verify, verifyAdmin} = auth;


//Routing Component
const router = express.Router();


//create a course POST
router.post("/", verify, verifyAdmin, courseController.addCourse)

//route for retrieving all courses
router.get("/all", courseController.getAllCourses);

//create a route for getting all active courses
//use default endpoint
//getAllActiveCourses
router.get("/", courseController.getAllActiveCourses);

//get a specific course
router.get("/:courseId", courseController.getCourse);

//edit a specific course
router.put("/:courseId", verify, verifyAdmin, courseController.updateCourse);

//G5 Activity Archive a course
router.put("/:courseId/archive", verify, verifyAdmin, courseController.archiveCourse);

//Activate a course
router.put("/:courseId/activate", verify, verifyAdmin, courseController.activateCourse);

// Route to search for courses by course name
router.post('/search', courseController.searchCoursesByName);

//Route to get all enrolled user to a certain course
router.get('/:courseId/enrolled-users', courseController.getEmailsOfEnrolledUsers);

// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;