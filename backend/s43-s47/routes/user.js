//Route
//Dependencies and Modules
const express = require("express");
const userController = require("../controllers/user");
const auth = require("../auth");
const { verify, verifyAdmin } = auth;


//Routing Component
const router = express.Router();

//Routes - POST
//Check Email
router.post("/checkEmail",(req,res)=>{
	userController.checkEmailExists(req.body).then(resultFromController=>res.send(resultFromController))
})

//Register a user
router.post("/register",(req,res)=>{
	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController))
})
//router.post("/register",userController.registerUser)


//User Authentication

router.post("/login",userController.loginUser);


//[ACTIVITY] Route for retrieving user details
router.post("/details", verify, userController.getProfile);

//Route to enroll user to a course
router.post("/enroll", verify, userController.enroll);

//[ACTIVITY]
router.get("/getEnrollments", verify, userController.getEnrollments);

// POST route for resetting the password
router.post('/reset-password', verify, userController.resetPassword);

// Update user profile route
router.put('/profile', verify, userController.updateProfile);


//[ACTIVITY S48] G5
router.post('/updateAdmin', verify, userController.updateUserAsAdmin);

/*// Define the route to update enrollment status
router.put('/update-enrollment-status', verify, verifyAdmin, userController.updateEnrollmentStatus);*/


//Export Route System
module.exports = router;