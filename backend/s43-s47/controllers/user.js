//controller
const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");

//Check if the email exists already
/*
	Steps:
	1. "find" mongoose method to find duplicate items
	2. "then" method to send a response back to the FE application based on the result of the "find method
*/

module.exports.checkEmailExists = (reqbody) =>{

	return User.find({email:reqbody.email}).then(result=>{

		//find method returns a record if a match is found
		if(result.length > 0){
			return true;
		}
		//no duplicate found
		//the user is not yet registered in the db
		else{
			return false;
		}
	})
}

//User Registration
/*
	Steps:
	1. create a new user object
	2. make sure that the password is encrypted
	3. save the new User to the database
*/

module.exports.registerUser = (reqbody) => {

	let newUser = new User({
		firstName: reqbody.firstName,
		lastName: reqbody.lastName,
		email: reqbody.email,
		mobileNo: reqbody.mobileNo,
		password: bcrypt.hashSync(reqbody.password,10)
	})

	return newUser.save().then((user,error)=>{

		if(error){
			return false;
		}else{
			return true;
		}
	})
	.catch(err=>err)
}


//User authentication
/*
	Steps:
	1. check the db if user email exists
	2. compare the password from req.body and password stored in the db
	3. Generate a JWT if the user logged in and return false if not
*/

module.exports.loginUser = (req,res)=>{

	return User.findOne({email:req.body.email}).then(result=>{
		if(result===null){
			return false;
		}else{
			//compareSync method is used to compare a non-encrypted password and from the encrypted password from the db
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password)
			//if pw match
			if(isPasswordCorrect){
				return res.send({access: auth.createAccessToken(result)})
			}
			//else pw does not match
			else{
				return res.send(false);
			}
		}
	})
	.catch(err=>res.send(err))
}


/*//G5 Activity
module.exports.getProfile = (req, res) => {
  User.findById(req.body.id)
    .then(user => {
      if (!user) {
        return res.send("No such user found.");
      }
      user.password = "";
      return res.send(user);
    })
    .catch(err => {
      console.error(err);
      return res.send(err);
    });
};*/


// Retrieve user details
/*
    Steps:
    1. Find the document in the database using the user's ID
    2. Reassign the password of the returned document to an empty string
    3. Return the result back to the frontend
*/
module.exports.getProfile = (req, res) => {

    return User.findById(req.user.id)
    .then(result => {
        result.password = "";
        return res.send(result);
    })
    .catch(err => res.send(err))

};

//Enroll user to a class
/*
	Steps:
	1. Find the document in the database using the user's ID
	2. Add the course ID to the user's enrollment array
	3. Update the document in the MongoDB Atlas Database
*/
module.exports.enroll = async (req, res) => {

	//to check in the terminal the user id and the courseId
	console.log(req.user.id);
	console.log(req.body.courseId);

	//checks if the user is an admin and deny the enrollment
	if(req.user.isAdmin){
		return res.send("Action Forbidden");
	}

	//Creates an "isUserUpdated" variable and returns true upon successful update otherwise returns error
	let isUserUpdated = await User.findById(req.user.id).then(user => {

		//Add the courseId in an object and push that object into the user's "enrollment" array
		let newEnrollment = {
			courseId: req.body.courseId
		}

		//Add the course in the "enrollments" array
		user.enrollments.push(newEnrollment);

		//Save the updated user and return true if successful or the error message if failed.
		return user.save().then(user => true).catch(err => err.message);

	});

	//Checks if there are error in updating the user
	if (isUserUpdated !== true){
		return res.send({message: isUserUpdated});
	}


	let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {

		let enrollee = {
			userId: req.user.id
		}

		course.enrollees.push(enrollee);

		return course.save().then(course => true).catch(err => err.message);
	})

	//Checks if there are error in updating the course
	if(isCourseUpdated !== true){
		return res.send({message: isCourseUpdated});
	}

	//Checks of both user update and course update are successful.
	if(isUserUpdated && isCourseUpdated){
		return res.send({message: "Enrolled Successfully."})
	}
}

/*//[ACTIVITY] Getting user's enrolled courses
module.exports.getEnrollments = (req, res) => {
	User.findById(req.user.id)
		.then(result => res.send(result.enrollments))
		.catch(error => res.send(error));
}*/

// Getting user's enrolled courses
module.exports.getEnrollments = (req, res) => {
	return User.findById(req.user.id).then(result => res.send(result.enrollments)).catch(err => res.send(err));
}


// Function to reset the password
module.exports.resetPassword = async (req, res) => {
  try {

  	// used object desctructuring
    const { newPassword } = req.body;
    const { id } = req.user; // Extracting user ID from the authorization header

    // Hashing the new password
    const hashedPassword = await bcrypt.hash(newPassword, 10);

    // Updating the user's password in the database
    await User.findByIdAndUpdate(id, { password: hashedPassword });

    // Sending a success response
    res.status(200).json({ message: 'Password reset successfully' });

  } catch (error) {

    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  }
};

// Mini Activity - Contextualize the updateProfile controller
// Controller function to update the user profile
module.exports.updateProfile = async (req, res) => {
  try {
    // Get the user ID from the authenticated token
    const userId = req.user.id;

    // Retrieve the updated profile information from the request body
    const { firstName, lastName, mobileNo } = req.body;

    // Update the user's profile in the database
    const updatedUser = await User.findByIdAndUpdate(
      userId,
      { firstName, lastName, mobileNo },
      { new: true }
    );

    res.json(updatedUser);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Failed to update profile' });
  }
}


//[ACTIVITY S48] G5

/*module.exports.updateAdmin = async (req, res) => {
  const { userId } = req.params;
  const { isAdmin } = req.user; // Assuming isAdmin is a property of the authenticated user

  if (!isAdmin) {
    return res.status(403).json({ message: 'You do not have permission to perform this action.' });
  }

  try {
    const updatedUser = await User.findByIdAndUpdate(userId, { isAdmin: true }, { new: true });

    if (!updatedUser) {
      return res.status(404).json({ message: 'User not found.' });
    }

    return res.json({ message: 'User updated successfully as admin.' });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: 'An error occurred while updating the user.' });
  }
};*/


module.exports.updateUserAsAdmin = async (req, res) => {
  const userIdToUpdate = req.body.userId; // Extract userId from the request body

  try {
    const updatedUser = await User.findByIdAndUpdate(
      userIdToUpdate,
      { isAdmin: true }, // Set isAdmin to true to update user as admin
      { new: true }
    );

    if (!updatedUser) {
      return res.status(404).json({ message: "User not found" });
    }

    res.json({ message: "User updated as admin successfully" });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Failed to update user as admin" });
  }
};


// UPDATE ENROLLMENT STATUS
/*module.exports.updateEnrollmentStatus = (req, res) => {
  const { userId, courseId, status } = req.body;

  // Perform logic to update enrollment status in your data storage (e.g., database)
  // Replace this with actual database operations

  res.status(200).json({ message: 'Enrollment status updated successfully' });
};*/


/*module.exports.updateEnrollmentStatus = async (req, res) => {
  const { userId, courseId, enrollmentStatus } = req.body;

  // Only allow admins to update enrollment status
  if (!req.user.isAdmin) {
    return res.status(403).json({ message: 'Access denied' });
  }

  try {
    const updatedUser = await User.findOneAndUpdate(
      { _id: userId, 
        'enrollments.courseId': courseId },
      { $set: { 'enrollments.$.status': enrollmentStatus } },
      { new: true }
    );

    if (!updatedUser) {
      return res.status(404).json({ message: 'User or enrollment not found' });
    }

    return res.status(200).json({ message: 'Enrollment status updated successfully' });
  } catch (error) {
    return res.status(500).json({ message: 'Internal server error' });
  }
};*/