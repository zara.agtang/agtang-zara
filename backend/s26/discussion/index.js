console.log("Array Traversal!");

//Arrays are used to store multiple related data/values in a single variable
//for us declare and create arrays, we use [] also known as "array literals"


let task1 = "Brush Teeth";
let task2 = "Eat Breakfast";

let hobbies = ["Play League","Read a Book", "Listen to music","Code"];
console.log(typeof hobbies);
//Arrays are actually a special type object
//Does it have key value pairs? Yes. index number : element
//Arrays make it easy to manager, manipulate as set of data
//Method is another term for functions associated with an object and is used to execute statements that are relevant to a specific object
/*
	Syntax:

	let/const arrayName = [elementA, elementB, elementC ...]
*/

//Common example of arrays
let grades = [98.5, 94.6, 90.8, 88.9];
let computerBrands = ['Acer','Asus','Lenovo','HP','Neo','Redfox','Gateway','Toshiba','Fujitsu'];

//This is not recommended
let mixedArr = [12,'Asus',null,undefined,{}];
let arraySample = ['Cardo','One Punch Man', 25000, false];

console.log(grades);
console.log(computerBrands);
console.log(mixedArr);
console.log(arraySample);

/*
	Mini-Activity

	1. Create a variable that will store an array of at least 5 of your daily routine in the weekends
	2. Create a variable which will store at least 4 capital cities in the world
	3. Log the variables in the console and send an ss in our hangouts (4 mins.)
*/

let tasks = ["Have breakfast", "Clean the house", "Bond with the family", "Watch movies", "Play games"];
let capitalCities = ["Tokyo", "Oslo", "Rome", "Seoul" ];

console.log(tasks);
console.log(capitalCities);

//Alternative way to write arrays
let myTasks = [

	'drink html',
	'eat javascript',
	'inhale css',
	'bake react'
];

//Create an array with values from variables:
let username1 = 'gdragon22';
let username2 = 'gdino21';
let username3 = 'ladiesman217';
let username4 = 'transformers45';
let username5 = 'noobmaster68';
let username6 = 'gbutiki78';

let guildMembers = [username1,username2,username3,username4,username5,username6];
console.log(guildMembers);
console.log(myTasks);

//[.length property]

//allows us to get and set the total number of items or elements in an array
//the .length property of an array gives a number data type

console.log(myTasks.length);//4
console.log(capitalCities.length);//4

let blankArr = [];
console.log(blankArr.length);//0

//length property can also be used with strings

let fullName = "Cardo Dalisay";
console.log(fullName.length);//13

//length property can also set the total number of items in an array, meaning we can actually delete the last item in the array or shorten the array by simply updating the length property of an array

//Re-assign
//length of myTasks = 4
//4 = 4-1;
myTasks.length = myTasks.length-1;
console.log(myTasks.length);//3
//This will output the myTasks except the last item
console.log(myTasks);

//to delete a specific item in an array, we can employ array methods

//another example using decrementation
capitalCities.length--;
//This will output the capitalCities except the last item
console.log(capitalCities);

//can we do the same with a string? NOOOOO!!
fullName.length = fullName.length-1;
console.log(fullName.length);//13

//To lengthen
//it adds an empty element
let theBeatles = ["John","Paul","Ringo","George"];
theBeatles.length++
console.log(theBeatles);//last element is empty

//to access the index 4 which is empty...
theBeatles[4] = fullName;
console.log(theBeatles);//5

//to add another element...
theBeatles[theBeatles.length] = "Tanggol";
console.log(theBeatles);

//[Read from Arrays]
/*
	Access elements with the use of their index

	Syntax:

	arrayName[index]
*/

console.log(capitalCities[0]);//accessed the first element of the array which is Tokyo

console.log(grades[100]);//undefined..there is no index 100

//To access elements...
let lakersLegends = ["Kobe","Shaq","Lebron","Magic","Kareem"];
console.log(lakersLegends[2]);//3rd element
console.log(lakersLegends[4]);//5th element
//you can also save array items  in another variable
//Store values from one array
let currentLaker = lakersLegends[2];
console.log(currentLaker);

//to change the index 2...
console.log("Array before assignment");
console.log(lakersLegends);
lakersLegends[2] = "Pau Gasol";
console.log('Array after re-assignment');
console.log(lakersLegends);


let favoriteFoods = [
	"Tonkatsu",
	"Adobo",
	"Pizza",
	"Lasagna",
	"Sinigang"
];

/*
	Mini Activity #2
	update/reassign the last two items in the array with your own personal faves
	log the favoriteFoods array in the console with its UPDATED elements using their index number
	send an ss in our hangouts
*/

favoriteFoods[3] = "Buffalo Wings";
favoriteFoods[4] = "Tinola";
console.log(favoriteFoods);


/*
	Mini Activity #3

	Create a function named findBlackMamba which is able to receive an index number as a single argument and return the item accessed by its index
		-function should be able to receive one argument
		-return the blackMamba accessed by the index
		-create a variable outside the function called blackMamba and store the value returned by the function in it
		-log the blackMamba variable (Kobe) in the console
*/

function findBlackMamba(index){
	return lakersLegends[index];
}

let blackMamba = findBlackMamba(0);
console.log(blackMamba);


/*
	Mini Activity #4
	-Create a function called addTrainers that can receive a singel argument
	-add the argument in the end of theTrainers array
	-invoke and add an argument to passed in the function
	-log theTrainer's array in the console
	-send a screenshot of your console in the Batch Hangouts

	Trainers to be added:   Brock
							Misty
*/

let theTrainers = ['Ash'];

function addTrainers(trainer){
	//use theTrainers.length to add elements
	theTrainers[theTrainers.length]=trainer;
}

addTrainers("Misty");
addTrainers("Brock");
console.log(theTrainers);

//Access the last element of an array
//since the first element of an array starts at 0, subtracting 1 to the length of the array will offset the value by one allowing us to get the last element

//5length-1 = index 4 (Kukoc)
let bullsLegends = ["Jordan","Pippen","Rodman","Rose","Kukoc"];
let lastElementIndex = bullsLegends.length-1;//Kukoc

console.log(bullsLegends[lastElementIndex]);
console.log(bullsLegends[bullsLegends.length-1]);

//Add items into the array

let newArr = [];
console.log(newArr[0]);//undefined

//in order to add another value
newArr[0] = "Cloud Strife";
console.log(newArr);

newArr[1] = "Tiffa Lockhart";
console.log(newArr);

//You could also use .length in adding another value
newArr[newArr.length] = "Barrett Wallace";
console.log(newArr);

//to change the last element (Barrett Wallace) in the array
newArr[newArr.length-1] = "Aerith Gainsborough";
console.log(newArr);

//Mutators
/*newArr.pop()
newArr.push()
newArr.shift()
newArr.unshift()
newArr.splice()*/


//[Loop over an array]
//it looped through from index 0 of newArr
for(let index = 0; index<newArr.length; index++){
	console.log(newArr[index])
}


let numArr = [5,12,30,48,40];

for(let index = 0; index<numArr.length; index++){
	//check if numArr is divisible by 5 or not
	if(numArr[index] % 5 === 0){
		console.log(numArr[index] + " is divisible by 5!")
	}else{
		console.log(numArr[index] + " is not divisible by 5!")
	}
}

//[Concept of Multidimensional Arrays]
//useful for storing complex data structures

let chessBoard = [
//index 0 vertically 
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],//index 0 horizontally
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];

console.log(chessBoard);


//access elements of a multidimensional array
console.log(chessBoard[1][4]);//e2
console.log("Pawn moves to: " + chessBoard[1][5]);//f2
console.log(chessBoard[7][0]);//a8
console.log(chessBoard[5][7]);//h6
console.log(chessBoard[0][0]);//a1