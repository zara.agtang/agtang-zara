//Servers can actually respond differently with different requests

//We start our request with our URL
//A client can start a different request with a different URL

//http://localhost:4000/ is not the same http://localhost:4000/profile

/*
	/ = url endpoint (default)
	/profile = url endpoint
*/

const http = require('http');
//creates a variable port to store the port number
const port = 4000;

const app = http.createServer((request,response)=>{
	/*
		Information about the URL endpoint of the req is in the request object
		Different request, require different response
		The process or way to respond differently to a request is called a ROUTE
	*/

	if(request.url == '/greeting'){
		response.writeHead(200,{'Content-type':'text/plain'})
		response.end('Hello, B297!')
	}

	else if(request.url == '/homepage'){
		response.writeHead(200,{'Content-type':'text/plain'})
		response.end('This is the homepage')
	}

	//MA
		//create an else condition that all other routes will return a message of 'Page Not Available'

	else {
		response.writeHead(404,{'Content-type':'text/plain'})
		response.end('Page Not Available, try other routes')
	}

})

app.listen(port);
console.log(`Server is now accessible at localhost: ${port}`)