//[MongoDB Aggregation]
/*
	Going to join data from multiple documents
	Aggregation gives us access to manipulate, filter, and compute for results providing us with information to make necessary development decisions without having to create a Frontend application
*/

//db - market_db
//collection - fruits

//In mongosh, you can create a db. You can simply use - use <db>
//In mongosh, you can create a collection. You can simply use - db.createCollection(<collectionName>)

	//Create a market_db database and fruits collection in MongoDB collection

	//Create documents to use for the discussion

	db.fruits.insertMany([

		{
			name: "Apple",
			color: "Red",
			stock: 20,
			price: 40,
			supplier_id: 1,
			onSale: true,
			origin:["Philippines","US"]
		},
		{
			name: "Banana",
			color: "Yellow",
			stock: 15,
			price: 20,
			supplier_id: 2,
			onSale: true,
			origin: ["Philippines","Ecuador"]
		},
		{
			name: "Kiwi",
			color: "Green",
			stock: 25,
			price: 50,
			supplier_id: 1,
			onSale: true,
			origin: ["US","China"]
		},
		{
			name: "Mango",
			color: "Yellow",
			stock: 10,
			price: 120,
			supplier_id: 2,
			onSale: false,
			origin: ["Philippines","India"]
		}
	])

//IN MONGOSH
//>use market_db
//>db.createCollection("vegetables")
//then paste the db.fruits


	//Use the aggregate method:
	/*
		Syntax:
			db.collectionName.aggregate([
				{ $match: {fieldA: value } },
				{ $group: {_id: "$fieldB"}, result: {operation} }
			])


		The $match is used to pass the documents that meet the specified condition(s) to the next pipeline stage/aggregation process
			Syntax:
				{$match: {field: value} }

		The $group is used to group elements together and field-value using the data from the group elements
			Syntax:
				{$group: { _id: "value", fieldresult: "valueResult" } }

		
	*/

		//Using both $match and $group along with aggregation will find for fruits that are onSale and will group the total amount of stocks per supplier

		db.fruits.aggregate([
			{$match:{onSale: true}},
			{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}}
		])

		//_id 1, total 45
		//_id 2, total 15

		//Field Projection with Aggregation
		/*
			The $project can be used when we are aggregating data to include/exclude fields from the returned results
			Syntax:
				{$project : {field:1/0}}
		*/

		db.fruits.aggregate([
			{$match:{onSale: true}},
			{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
			{$project: {_id: 0}}
		])

		//total 15
		//total 45

		//Sorting aggregated results
		/*
			The $sort can be used to change the order of aggregated results
			Providing the value of -1, we will sort the aggregated results in reverse order
				Syntax:
					{$sort: {field: 1/-1}}
			//1 (ascending order)
			//-1 (descending order)
		*/


		db.fruits.aggregate([
			{$match:{onSale: true}},
			{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
			{$sort: {total: -1}}
		])
		//total 45 (descending from large to small)
		//total 15


		db.fruits.aggregate([
			{$match:{onSale: true}},
			{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
			{$sort: {total: 1}}
		])
		//total 15 (ascending from small to large)
		//total 45


		//Aggregate values based on array fields
		/*
			The $unwind deconstructs an array field from a collection/field with an array value to output a result for each element
			Syntax:
				{$unwind: field}
		*/

		db.fruits.aggregate([
			{$unwind: "$origin"}
		])
		//We have now 8 separate results based on array elements


		//$sum calculates and gives the collective sum of number values
		//The 1 passed in $sum is used to increment the count for each group by 1

		db.fruits.aggregate([
			{$unwind: "$origin"},
			{$group: {_id: "$origin", kinds: {$sum: 1}}}
		])


		//MA1: Find how many fruits are yellow
			//Use $match and $count

		db.fruits.aggregate([
			{$match: {color: "Yellow"}},
			{$count: "yellowFruits"}
		])


		//Using regex
		db.fruits.aggregate([
			{$match: {color: {$regex:'yElloW',$options:'i'}}},
			{$count: "yellowFruits"}
		])


		//MA2: find how many fruit stocks that are less than or equal to 10
		//use $match and $count

		db.fruits.aggregate([
			{$match: {stock: {$lte: 10}}},
			{$count: "lowOnStocks"}
		])


		//MA3: find fruits that are on sale for each group of supplier that has the maximum stocks
		//use $match and $group
			//use $max for the stocks

		db.fruits.aggregate([
			{$match:{onSale: true}},
			{$group: {_id: "$supplier_id", maxStock: {$max: "$stock"}}}
		])


		//Supplementary Notes
		/*
			1. $sum - Returns a sum of numerical value. Ignores non-numeric values.
			2. $max - Returns the highest value for each group.
			3. 
		*/