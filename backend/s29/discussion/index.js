console.log("Hi from index.js");

//fetch() method has one argument by default, the URL
//the url is a representative address of accessing a resource/data in another machine
//for now, we will use the jsonplaceholder url, which is a sample server where we can get data from
//the .then() method will allow us to process the data we retrieve using fetch in another function
//the response is simply parameter. However, as a convention, indicating that we are now going to process the response from our server
//It is a representation of what we "got" from the server.
//reponse.json() is a method to convert the incoming data as a proper JS object we can further process

//we can add another .then() method to do something with the processed server response

//CRUD - Create, Read, Update, Delete

//get //read or retrieve
fetch("https://jsonplaceholder.typicode.com/posts") .then(response=>response.json()).then(data=>{
	//console.log(data)
	//this will allow us to trigger the showPosts() function after we fetch the data from our server
	showPosts(data);
})

//const showPosts = function (posts){}

//Receive the fetched data as an argument
const showPosts = (posts) =>{

	console.log(posts);

	//add each post as a string
	let postEntries = '';

	posts.forEach((post)=>{

		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>

		`
	})

	//console.log(postEntries);

	document.querySelector('#div-post-entries').innerHTML = postEntries;

}

//by default the page is refreshed upon form submission event, to prevent this, we can pass the event parameter to our function and use the preventDefault() method
document.querySelector('#form-add-post').addEventListener('submit',(event)=>{
	event.preventDefault();

	let titleInput = document.querySelector("#txt-title");
	let bodyInput = document.querySelector("#txt-body");
	/*console.log(titleInput.value);
	console.log(bodyInput.value);
	console.log("Hello! The form has been submitted!")*/

	//whenever we try to add, update, and delete to a server, we have to pass antother argument to the fetch() method that also contains other details
	//Syntax
		//fetch("<URL>",{options})
			//options object should look like this:
		//method: this property tells the server what we intend to do, the value passed here are what we call the HTTP Methods
			//GET(retrieve/read): For getting data in a server
			//POST(add): For add data in a server, we will use POST this time because want to ADD data to our server
			//PUT(update): For updating data in a server
			//DELETE(delete): For deleting data in a server

			//body: this property contains the main content that we want to send to our servers
			//headers: this property contains other details that we need to send to our server

	fetch("https://jsonplaceholder.typicode.com/posts",{
		method: 'POST',
		body: JSON.stringify({
			title: titleInput.value,
			body: bodyInput.value,
			userId: 1
		}),
		headers: {'Content-type':'application/json'}
	})
	.then((response)=>response.json())
	.then((data)=>{
		console.log(data);
		alert('Successfully added!')

		//clear the input element after submission
		titleInput.value = null;
		bodyInput.value = null;
	})

	alert('Successfully added!');
})

//Edit Post

const editPost = (id) =>{

	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
	document.querySelector('#btn-submit-update').removeAttribute('disabled');
}

//Update Post

document.querySelector('#form-edit-post').addEventListener('submit',(e)=>{
	e.preventDefault();

	fetch("https://jsonplaceholder.typicode.com/posts/1",{
		method: 'PUT',
		body: JSON.stringify({
			id: document.querySelector('#txt-edit-id').value,
			title: document.querySelector('#txt-edit-title').value,
			body: document.querySelector('#txt-edit-body').value,
			userId: 1
		}),
		headers: {'Content-type':'application/json'}
	})
	.then((response)=>response.json())
	.then((data)=>{
		console.log(data);
		alert('Successfully updated!')

		//clear the input element after submission
		document.querySelector('#txt-edit-id').value = null;
		document.querySelector('#txt-edit-title').value = null;
		document.querySelector('#txt-edit-body').value = null;
		document.querySelector('#btn-submit-update').setAttribute('disabled',true);
	})
})

//Activity

const deletePost = (id) =>{
	fetch('https://jsonplaceholder.typicode.com/posts',{method: 'DELETE'});
	document.querySelector(`#post-${id}`).remove();
}

document.querySelector(`#delete-all`).addEventListener('click',()=>{
	document.querySelector(`#div-post-entries`).innerHTML = '';
	alert("All Posts Deleted!!!!!")
})