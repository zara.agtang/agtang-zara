console.log("DOM Manipulation!");

console.log(document);//this accessed the HTML file
console.log(document.querySelector('#clicker'));//accessed the object in the document (button element)

/*
	document - refers to the whole webpage
	querySelector - used to select a specific element (obj) as long as it is inside the html tag (html element)

	- takes a string input that if formatted like CSS selector
	-it can select elements regardless if the string is an id, class, or tag as long as the element is existing in the webpage
*/

/*
	Alternative methods that we use aside from querySelector in retrieving elements:
		document.getElementById()
		document.getElementByClassName()
		document.getElementByTagName()
*/

let counter = 0;

const clicker = document.querySelector('#clicker');

	clicker.addEventListener('click',()=>{
		
		console.log("The button has been clicked");
		counter++;
		alert(`The button has been clicked ${counter} times`);
	})

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");


//const updateFullName = function(e){}

//This code displays your full name when inputted
const updateFullName = (e) =>{

	let firstName = txtFirstName.value;
	let lastName = txtLastName.value;

	spanFullName.innerHTML = `${firstName} ${lastName}`
}

	txtFirstName.addEventListener('keyup', updateFullName);
	txtLastName.addEventListener('keyup', updateFullName);


/*txtFirstName.addEventListener('keyup',(event)=>{
	//innerHTML = property that captures the element like in the (txtFirstName.value)
	spanFullName.innerHTML = txtFirstName.value
})*/

/*const labelFirstName = document.querySelector("#label-txt-first-name");

labelFirstName.addEventListener('mouseover',(e)=>{

	//alert("You hovered the First Name Label!")
})*/

//Mini Activity

/*
	Add first name then add code for the last name
*/

