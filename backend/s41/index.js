//Import express
const express = require("express");
//Import mongoose
const mongoose = require('mongoose');

const app = express();
const port = 3001;

//[Section] Creation of todo list routes

//Allows your app to read json data
app.use(express.json());
//Allows your app to read data from forms
app.use(express.urlencoded({extended: true}))


//Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://zaraagtang:admin123@batch-297.wldzis1.mongodb.net/taskDB?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});


//Set notifications for connection success or failure
//Connection to the database
//Allows to handle errors when the initial connection is established
//Works with the on and once Mongoose methods
let db = mongoose.connection;

//If a connection error occured, output in the console
// console.error.bind(console) allows us to print errors in the browse console and in the terminal
//"connection error" is the message that will display if an error is encountered
db.on("error", console.error.bind(console, "connection error"));

//If the connection is successful, output in the console
db.once("open", () => console.log("We're connected to the cloud database"))

//Schemas determine the structure of the documents to be written in the database
//Schemas act as a blueprint to our data
//Use the Schema() constructor of the Mongoose module to create a new Schema object
//The "new" keyword creates a new Schema
const taskSchema = new mongoose.Schema({

	//Define the fields with the corresponding data type
	//For a task, it needs a "task name" and "task status"
	//There is a field called "name" and its data type is "String"
	name: String,
	//There is a field called "status" that is a "String" and the default value is "pending"
	status: {
		type: String,
		default: "pending"
	}
});


//[Section] Models
//Uses schemas and are used to create/instatiate objects that correspond to the schema
//Models use Schemas and they act as the middleman from the server (JS code) to our database
//Server > Schema (blueprint) > Database > Collection
//The variable/object "Task" can now be used to run commands for interacting with our database
const Task = mongoose.model("Task", taskSchema)



app.post("/tasks", (req, res) => {
	//"findOne" is a Mongoose method that acts similar to "find" of MongoDB
	//findOne() returns the first document that matches the search criteria as a single object

	Task.findOne({name: req.body.name}).then((result, err) => {

		//If a document was not found and the document's name matches the information sent via the client/Postman
		if(result != null && result.name == req.body.name){
			return res.send("Duplicate task found");

		//If no document was found
		} else {
			//Create a new task and save it to the database
			let newTask = new Task({
				name: req.body.name
			});

			//the save() method will store the information to the database
			//since the "newTask" was created/instantiated from the Mongoose Schema it will gain access to this method to save the database
			//The "save()" method can send the result or error in another JS method called then()
			newTask.save().then((savedTask, saveErr) => {
				if(saveErr){
					//if error occurs it log the error
					return console.error(saveErr);
				} else {
					//return a status code of 201 for created
					//Sends a message "New task created" on successful creation
					return res.status(201).send("New task created");
				}
			})
		}
	})
})


//Get all of our tasks
app.get("/tasks", (req, res) => {
	//"find" is a Mongoose method that is similar to MongoDB "find", and an empty "{}" means it returns all the documents and stores them in the "result" parameter of the callback function
	Task.find({}).then((result, err) => {
		if(err){
			return console.log(err);
		} else {
			return res.status(200).json({
				data:result
			})
		}
	})
})





// Listen to the port, meaning, if the port is accessed, we will run the server
if(require.main === module){
	app.listen(port, () => console.log(`Server running at port ${port}`));
}

module.exports = app;